def find_it(seq):
    nums = {}
    for num in seq:
        nums = count_int(nums, num)
    return filter_odd(nums)


def count_int(nums, num):
    count = 1
    if num in nums.keys():
        count += nums[num]
    nums[num] = count
    return nums


def filter_odd(nums):
    for key, value in nums.items():
        if value % 2 != 0:
            return key
    return None
